<?php
/*
Plugin Name: MMD Taxonomy Colors
Plugin URI: http://theaveragedev.com/plugins/mmd-taxonomy-colors.
Description: The plugin allows each taxonomy term to be given its own colors. Thanks to Bainternet, Ohad Raz for his code.
Version: 1.0
Author: theAverageDev, Luca Tumedei
Author URI: http://www.theaveragedev.com
*/

//include the main class file
require_once("Tax-meta-class/Tax-meta-class.php");

if (is_admin()){

  /*
   * configure your meta box
   */
  $config = array(
    'id' => 'mmd_tax_colors_meta_box',          // meta box id, unique per meta box
    'title' => 'Color',          // meta box title
    'pages' => array('category'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => false,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => false          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
    );

  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);

  /*
   * Add fields to your meta box
   */
  $my_meta->addColor('mmd_color_field_id',array('name'=> __('Category Color','tax-meta')));

  //Finish Meta Box Decleration
  $my_meta->Finish();
}

function the_category_color($term_id){
	$cat_color = get_tax_meta($term_id, 'mmd_color_field_id');
  if ($cat_color == '') {
    echo '#222222';
  }
  else echo $cat_color;
}

function get_the_category_color($term_id){
  $cat_color = get_tax_meta($term_id, 'mmd_color_field_id');
  if ($cat_color == '') {
    return '#222222';
  }
  else return $cat_color;
}

?>
